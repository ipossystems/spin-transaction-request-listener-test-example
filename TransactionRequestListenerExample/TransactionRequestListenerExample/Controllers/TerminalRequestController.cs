﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Web.Http;
using TransactionRequestListenerExample.Models;

namespace TransactionRequestListenerExample.Controllers
{
    [RoutePrefix("api/terminalRequest")]
    public class TerminalRequestController : ApiController
    {
        [HttpPost]
        [Route("RequestTransaction")]
        public void RequestTransaction(TransactionRequestModel request)
        {
            Write(JsonConvert.SerializeObject(request));
        }
        
        [HttpPost]
        [Route("InvoiceData")]
        public void InvoiceData(InvoiceDataModel request)
        {
            Write(JsonConvert.SerializeObject(request));
        }
        
        private void Write(string message = null)
        {
            try
            {
                var path = ConfigurationManager.AppSettings["PathToLogFile"];

                using (StreamWriter w = File.AppendText(!string.IsNullOrEmpty(path) ? path : "trle.log"))
                {
                    w.WriteLine("{0}: {1}",
                        DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss.fffff"), message);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
