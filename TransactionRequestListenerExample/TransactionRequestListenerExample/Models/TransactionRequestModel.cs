﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TransactionRequestListenerExample.Models
{
    public class TransactionRequestModel
    {
        public string Tpn { get; set; }

        [JsonProperty("clerk_id")]
        public long? ClerkId { get; set; }

        [JsonProperty("table_num")]
        public long? TableNum { get; set; }

        [JsonProperty("ticket_num")]
        public long? TicketNum { get; set; }

        [JsonProperty("guest_num")]
        public long? GuestNum { get; set; }

        public string Key { get; set; }

        public string RegisterId { get; set; }

        public string AuthKey { get; set; }

        [JsonProperty("get_list")]
        public bool? GetList { get; set; }

        [JsonProperty("server_num")]
        public long? ServerNum { get; set; }

        [JsonProperty("invoice_num")]
        public long? InvoiceNum { get; set; }

        public string Login { get; set; }
    }
}