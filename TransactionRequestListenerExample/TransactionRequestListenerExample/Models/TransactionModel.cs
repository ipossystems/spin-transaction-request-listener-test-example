﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TransactionRequestListenerExample.Models
{
    public class TransactionModel
    {
        public string PaymType { get; set; }
        public string TransType { get; set; }
        public string InvNum { get; set; }
        public string RefId { get; set; }
        public string Amount { get; set; }
        public string Tip { get; set; }
        public string Untipped { get; set; }
        public string BatchNum { get; set; }
    }
}