﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TransactionRequestListenerExample.Models
{
    public class InvoiceDataModel
    {
        public List<TransactionModel> Trans { get; set; }
        public string Status { get; set; }
        public string RefId { get; set; }
        public string RegId { get; set; }
        public string Tpn { get; set; }
    }
}