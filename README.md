# SPIn Transaction Request Example Applications 
The example service listens to requests from a SPIn Proxy and saves them to a log.
This service has to be available for external requests from the Internet.

## Introduction

Transaction request it is a function of Dejavoo terminals which allow to initialise a payment transaction from external POS system by some parameters.

The feature of a transaction request is necessary to use, when Software at POS manages information about transaction parameters, but need to call this transaction from a terminal side.
E.g. closing a bill in restaurant when a software has information about a bill and need to choose a table and call a sale transaction on terminal:

1. A waiter uses “transaction request” feature on terminal to close a bill for a particular table.
2. A terminal sends a message with a number of table to SPIn Proxy
3. SPIn Proxy has a connection to terminal and to software at restaurant and a route for sending message between them.
3. A restaurant software sends a transaction to SPIn Proxy for a terminal.
4. SPIn Proxy forwards a transaction request to terminal.
5. Terminal starts transaction and requests a credit card. After a transaction was submitted to host, a 6. terminal sends a result of transaction via Proxy into a restaurant system.

A connection between a terminal and the SPIn Proxy server is implemented and supported automatically.	
To use a "Transaction request" need to implement a connection between SPIn Proxy and Software at POS.

## WEB API
When user call a transaction request on terminal and fill any Prompt about transition details proxy sends request in JSon format to External System 

Example of request which SPIn Proxy send to the service:

```sh
{
	  "tpn":"Z6testtpn01",
	  "Clerk_id":1,
	  "Table_num":2,
	  "Ticket_num":3,
	  "Guest_num":0,
	  "Key":"",
	  "RegisterId":"790160132",
	  "AuthKey":"dOQnlHfc11",
	  "TrId":"06be146b-45a1-497a-b43e-aa3255a77dec"
}
```

Where:

Value | Type | Comments
----- | ---- | --------
tpn | string | TPN
Clerk_id | int | Value of prompt “Clerk”
Table_num | int | Value of prompt “Table”
Ticket_num | int | Value of prompt “Ticket”
Guest_num | int | Value of prompt “Guest”
Key | string | Not mandatory(unused) field
RegisterId | string | CashRegister ID on Proxy
AuthKey | string | Proxy AuthKey
TrId | string | Contents unique ID of request

## Settings of terminal parameters
To connect terminal to SPIn Proxy server to use transaction request feature need to set next parameter:
Application: **DvAmApp** 
Section: **CoreWs**
Parameters:
```sh
CGI_Host_System_IP				        spinpos.net
CGI_Host_System_Port				    80
CGI_Host_System_URL				        spin/integration.asmx
CGI_Host_System_Security			    Off (by default)
CGI_Host_System_SslServerProfile	    ca-bundle.crt (by default)
CGI_Host_System_Ssl_Verify_Peer		    On (by default)
CGI_Host_System_Heartbeat_Timeout	    120 (by default)
CGI_Transaction_Request_Enabled		    On
CGI_Transaction_Request_Enabled		    On / Off
Prompt_Clerk					        Off / On / Required
Prompt_Table					        Off / On / Required
Prompt_Ticket					        Off / On / Required
Prompt_Guest					        Off / On / Required
```

These parameters might be set in two ways:
- via [STEAM](https://dvmms.com/steam) configuration system - need to have a roles of Manager/Technical Service or request a help of support team
- via [DeNovo](https://dvmms.com/denovo) wizard on a terminal details page